# software construction
requirements:
- construction is deterministic
  - for a given set of inputs, we always get the same output
  - all dependencies are known and controlled
  - host tools and host configuration do not affect the output
- cycle-time is minimised
  - we don't waste time constructing artifacts that already exist
  - we don't put artificial constraints in place which extend cycle time unnecessarily
  - we monitor elapsed construction times, and make optimisations to minimise them
- setup time is minimised
  - setup process is obvious and fast (e.g. a single install command, or login to shared service)
  - setup process is known to work, and installs the correct version(s) of everything
  - setup process results in engineer being able to work efficiently and smoothly
  - setup process includes provision of local debugging tools
- learning time is minimised
  - we use tools that are obvious and provide clear, fast feedback
  - documentation is current, clear, helpful, sufficient, concise, and ideally complete.
- debugging time is minimised
  - tools are instrumented to support fast and effective debugging
  - tools behaviour is reliable (ideally deterministic)
- construction works for all targets
   - friction and delta for all targets is minimised
   - we use the same tooling/process/approach/configuration for all targets
   - reuse of inputs is maximised for all targets
- usually there is one canonical workflow for construction
  - which satisfies all of the above requirements, with evidence

problems:
- construction takes too long, so
  - engineer time is wasted directly waiting for construction
  - downstream/dependent activities are delayed

- construction process/tooling is complex and/or involves multiple approaches, so
  - engineer time is wasted learning specifics and tricks and workarounds
  - new and drive-by engineers waste time getting up-to-speed
  - engineers are unable to develop locally for whatever reason, increasing
    cycle-time unreasonably

- construction tools misbehave in unexpected ways, so
  - engineer time is wasted debugging the construction tooling
  - downstream/dependent activities are delayed
  - downstream/dependent activities may be impacted by pollution/corruption

- construction tools generate different outputs on different machines, or between runs, so
  - engineer time is wasted debugging the differences
  - we can not guarantee that the output is what we expect/want
  - we can not guarantee that we can get the same output again in future
  - downstream/dependent activities are delayed
  - downstream/dependent activities may be impacted by changes/pollution/corruption

- constructions are affected/modified by dependencies/inputs/upstream changes, so
  - engineer time is wasted debugging the differences
  - we can not guarantee that the output is what we expect/want
  - we can not guarantee that we can get the same output again in future
  - downstream/dependent activities are delayed
  - downstream/dependent activities may be impacted by changes/pollution/corruption

- construction tools make it difficult/impossible to create the right/best outputs, so
  - we compromise and settle for suboptimal outputs
  - downstream activities are forced to deal with suboptimal inputs

- versioning and metadata is mishandled, leading to labeling errors and confusion, so
  - engineer time is wasted debugging the differences
  - downstream/dependent activities may be impacted by changes/pollution/corruption

- construction outputs are not collected, and/or not measured, and/or not comparable
  - time and effort is wasted establishing what happened
