# t.software

- t.software is composed of one or more t.changes
- we assess the trustability of t.software based on t.evidence
- t.software MUST have t.requirements
- t.software MUST satisfy its t.requirements
- there MUST be t.measurable t.evidence that t.software satisfies its t.requirements

# t.change

- a t.change is is any change to t.standards, t.requirements, t.tests, t.inputs,
  t.documentation, or other materials that affect t.software
- a t.change specifically includes the initial upload of any component of any
  materials

# t.requirements

  - each t.requirement MUST be clear/unambiguous
  - each t.requirement MUST be verifiable/confirmable
  - each t.requirement MUST be unitary/cohesive
  - each t.requirement MUST be traceable from need to implementation via t.evidence
  - each t.requirement MUST be current
  - each t.requirement MAY be graded by priority/importance
  - the set of t.requirements MUST be complete
    - known omissions and assumptions MUST be stated
  - the set of t.requirements MUST be consistent/coherent

# t.tests

- t.requirements MUST exist
- all t.requirements MUST have t.tests
- all t.tests MUST pass
- the scope of the t.requirements MUST be t.measurable
- the scope of the t.tests MUST be t.measurable
- we MAY t.estimate the scale or scope of missing t.requirements
- we MAY t.estimate the scale or scope of missing t.tests

# t.evidence

- t.evidence is machine-parseable metadata associated with t.software
- t.evidence MUST include t.evidence of its own integrity/correctness
- t.evidence MUST include t.identity for author(s)
- t.evidence MUST include t.identity for committer/uploader
- t.evidence MUST include creation/modification/commit time-and-date
- t.evidence MAY include text description and/or comments
- t.evidence MAY include t.identity for reviewer(s)
- t.evidence MAY include t.identity for review comments


# t.identity, t.identify

- t.identity is a kind of t.evidence
- t.identity MUST map to a single t.contributor
- a t.contributor is a uniquely identifiable person, organisation or software program
- t.contributors contribute to t.software and/or t.evidence
- t.contributors are responsible and accountable for their contributions

# t.measure, t.measureable, t.estimate, t.measurement, t.metrics

- t.measurements MUST be derived from t.evidence via automation
- t.estimates MUST be derived from t.evidence via automation
- t.measurements MUST be believed to be factually correct
- t.estimates are known to be uncertain
- we t.measure all available t.evidence about t.software, producing t.metrics
- we t.measure multiple t.metrics, including signals for some (ideally all) of
  - engineering cost/effort
  - project scale
  - project value
  - project quality
  - project software performance/efficiency
- we use t.estimates to check t.measurements or fill in gaps in t.evidence
- we look conflicts between t.metrics
  - we evaluate the conflicts
- we aim to identify gaps in t.metrics
  - we seek new t.evidence to close them
- all t.metrics MUST be meaningful
  - for t.software, all t.metrics are acceptable
- we can aggregate/manipulate t.metrics to provide a t.measure of trustability

# t.provenance

- t.evidence MUST t.identify the authors of the t.software
- t.evidence MUST t.identitify the reviewers of the t.software
- t.evidence MUST t.identify the integrators/mergers of the t.software
- FIXME add hypothesis provenance logic here

# t.build

- a t.build consists of any compilation, reformation, rearrangement,
  or copying of any t.input to generate any t.artefacts that may become
  subject to test or deployment
- FIXME add hypothesis build logic here

# t.input

- a t.input is any source code or binary artefact used by a t.build
- the environment in which a t.build is run is also a t.input
- a t.artefact from one t.build may be a t.input to another t.build
- a t.input MUST be uniquely identifiable

# t.artefact

- a t.artefact is any output from a t.build (including logs or other
  material) which is retained to be used as input to another t.build,
  as part of a release of t.software, or as t.evidence
- a t.artefact MUST have t.evidence of the t.inputs and t.build used
  to construct it in a t.reproducible manner

# t.process, t.effect, t.friction, t.value

- t.process is a set of practices to increase the trustability of software
- adding t.process to a project MUST lead to t.measurable t.effects
- t.effects are t.measures and/or t.metrics
- t.friction is negative t.effects
- t.value is positive t.effects
- adding t.process to a mature (widely trusted) project must
  - provide t.measurable t.value
  - involve acceptable (minimal) t.friction
  - be justifiable to be used upstream and/or
  - be applicable downstream

# t.reproducible t.reproducibility

- t.reproducibility is the property of any t.artefact to be re-creatable on
  demand
- any two constructions of t.software by the same t.build actions should result
  in bit-for-bit equivalent t.artefacts
- any such two constructions must be able to be separated in time, or be
  performed on different (but equivalent) host systems.
- FIXME add hypothesis reproduction logic here

# t.function

- t.function is a definition of the functional behaviour of t.software
- each t.function MUST be defined by t.requirements
- each t.function MUST be validated by t.tests
- FIXME add hypothesis functionality logic here

# t.update

- t.software exists in a world where changes to t.requirements will occur after
  any given release of the t.software.
- as such, all t.software MUST be capable of in-field t.update
- t.update MAY refer to all t.software on a system, or to any well-defined
  subset thereof
- a t.update MUST be atomically applicable with rollback capability
- a t.update MUST be cryptographically validatable prior to t.software
  consuming it.
- FIXME add hypothesis update logic here

# t.context

- t.context is written information which states or clarifies relevant ideas for t.software
- [trustable software discussions](https://lists.trustable.io/pipermail/trustable-software/)
- [wikipedia requirement](https://en.wikipedia.org/wiki/Requirement)
- [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt)
- virtually all existing software does not satisfy the [trustable software hypothesis](https://gitlab.com/trustable/overview/wikis/hypothesis-for-software-to-be-trustable)
  - but a lot of software is widely trusted
- to be useful, the t.software concepts must be
  - easy to understand
  - justifiable
  - practical and realistic
  - applicable to existing software
    - collecting t.evidence needs to be low friction
    - t.measures and t.metrics for widely trusted existing software should be high
