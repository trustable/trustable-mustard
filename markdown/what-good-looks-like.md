# What good looks like:

Everything is:
- documented in writing, declaratively
  - as sentences/statements
  - or as configuration
  - or as scripts
  - at a single source of truth
  - visible to all contributors
- version-controlled
  - with effective branch-and-merge
  - with contributor identification and accountability
  - with evidence of independent pre-merge reviews
- cycle-time optimised
- originated as an issue/change
- specified and designed for availability and scale from the start

Issues/changes state:
- what the problem/requirement is
- what the expected solution is
- how the solution is implemented
- how the solution is verified/tested
- how the solution is landed/integrated

Construction is:
- declarative
- deterministic
- reproducible (at least functionally, ideally bit-for-bit)
- traceable with provenance and custody
- via shared ci pipeline infrastructure
- including all dependencies

Deployment + operation is:
- scripted/automated from the construction pipeline, including
  - atomic update
  - atomic rollback
  - atomic recovery
